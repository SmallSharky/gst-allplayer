# Build

```
# create build directory, make it current
mkdir build
cd build

# build
cmake .. -G Ninja
ninja
```
